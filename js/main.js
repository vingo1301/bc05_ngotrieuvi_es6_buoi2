let index = -1;
let dataGlasses = [
    { id: "G1", src: "./img/g1.jpg", virtualImg: "./img/v1.png", brand: "Armani Exchange", name: "Bamboo wood", color: "Brown", price: 150, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? " },
    { id: "G2", src: "./img/g2.jpg", virtualImg: "./img/v2.png", brand: "Arnette", name: "American flag", color: "American flag", price: 150, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G3", src: "./img/g3.jpg", virtualImg: "./img/v3.png", brand: "Burberry", name: "Belt of Hippolyte", color: "Blue", price: 100, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G4", src: "./img/g4.jpg", virtualImg: "./img/v4.png", brand: "Coarch", name: "Cretan Bull", color: "Red", price: 100, description: "In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G5", src: "./img/g5.jpg", virtualImg: "./img/v5.png", brand: "D&G", name: "JOYRIDE", color: "Gold", price: 180, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?" },
    { id: "G6", src: "./img/g6.jpg", virtualImg: "./img/v6.png", brand: "Polo", name: "NATTY ICE", color: "Blue, White", price: 120, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G7", src: "./img/g7.jpg", virtualImg: "./img/v7.png", brand: "Ralph", name: "TORTOISE", color: "Black, Yellow", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam." },
    { id: "G8", src: "./img/g8.jpg", virtualImg: "./img/v8.png", brand: "Polo", name: "NATTY ICE", color: "Red, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim." },
    { id: "G9", src: "./img/g9.jpg", virtualImg: "./img/v9.png", brand: "Coarch", name: "MIDNIGHT VIXEN REMIX", color: "Blue, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet." }
];
let renderGlassList = () => {
    let contentHTML = "";

    dataGlasses.forEach(function(item){
    index++;
    contentHTML += `
    <div class="col-4"><img onclick="doiKinh(${index})" src="${item.src}" alt="" class="vglasses__items" /></div>
    `;
    })
    document.getElementById("vglassesList").innerHTML = contentHTML;
}
renderGlassList();
let doiKinh = (index) => {
    renderSanPham(index);
    contentHTML = `<img src="${dataGlasses[index].virtualImg}" alt="" />`
    document.getElementById("avatar").innerHTML = contentHTML;
}
let renderSanPham = (index) => {
    contentHTML = `
    <p>${dataGlasses[index].brand} - ${dataGlasses[index].name}</p>
    <p>${dataGlasses[index].price}$ - ${dataGlasses[index].color}</p>
    <p>${dataGlasses[index].description}</p>
    `
    document.getElementById("glassesInfo").innerHTML = contentHTML;
    document.getElementById("glassesInfo").style.display = 'block';
}
let removeGlasses = (boolean) => {
    if (boolean){
        index++;
    }
    else{
        index--;
    }
    if(index < 0){
        index = 8;
    }
    if(index > 8){
        index = 0;
    }
    doiKinh(index);

}
